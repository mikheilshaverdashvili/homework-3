package com.example.homework3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val items = ArrayList<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter

    private var titleText = "title"
    private var descriptionText = "description"
    private var dateNumber = "date"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        addItemButton.setOnClickListener {
            addItemOpen()
            adapter.notifyItemInserted(items.size - 1)
            recyclerview.scrollToPosition(items.size - 1)
        }

        addInfo()

        dataSet()
        recyclerview.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items, this)
        recyclerview.adapter = adapter
    }
    private fun dataSet() {
        items.add(ItemModel(R.mipmap.forest, "title", "description", "2020-01-22 21:00"))
        items.add(ItemModel(R.mipmap.waterfall, "title", "description", "2018-06-25 00:21"))
        items.add(ItemModel(R.mipmap.dog, "title", "description", "2015-07-08 08:32"))
        items.add(ItemModel(R.mipmap.mountain, "title", "description", "2011-03-21 15:05"))
        items.add(ItemModel(R.mipmap.green, "title", "description", "2014-09-12 12:24"))
        items.add(ItemModel(R.mipmap.bridge, "title", "description", "2013-01-15 15:20"))
    }

    private fun addItemOpen() {
        val intent = Intent (this, AddItem::class.java)
        startActivity(intent)
    }

    private fun addInfo() {
        val intent = intent.extras
        if (intent != null) {
            titleText = intent!!.getString("title", "title")
            descriptionText = intent.getString("description", "description")
            dateNumber = intent.getString("date", "date")
            items.add(ItemModel(R.mipmap.forest, titleText, descriptionText, dateNumber))
        }
    }

}
