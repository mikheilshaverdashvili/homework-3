package com.example.homework3

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_addinfo.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class AddItem : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) { super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addinfo)
        init()
    }

    private fun init() { val currentDateTime = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
        val formatted = currentDateTime.format(formatter)
        dateNew.text = formatted.toString()
        save.setOnClickListener {
            if (titleNew.text.toString().isEmpty() || descriptionNew.text.toString().isEmpty()) {
                Toast.makeText(this, "Please feel all fields", Toast.LENGTH_SHORT).show()
            }
            else {
                openMainActivity()
            }
        }
    }

    private fun openMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("title", titleNew.text.toString())
        intent.putExtra("description", descriptionNew.text.toString())
        intent.putExtra("date", dateNew.text.toString())

        startActivity(intent)
    }


}